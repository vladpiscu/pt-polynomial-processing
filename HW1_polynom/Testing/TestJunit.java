package Testing;
import static org.junit.Assert.*;
import org.junit.Test;

import Polynomial.Monom;
import Polynomial.Polynom;

public class TestJunit {
	
	@Test
	 public void test() {
	      //test data
	      Monom m1 = new Monom("+3x^5");
	      Monom m2 = new Monom("-7x^5");
	      Monom m3 = new Monom("+1x^6");
	      

	      //check for equality
	      assertEquals("+3x^5", m1.toString());
	      
	      //check for false condition
	      assertTrue(m1.equals(m2));
	      assertFalse(m1.equals(m3));
	      
	    //test data
	      Polynom p1 = new Polynom("+3x^5+1x^3+2x^1+4x^0");
	      Polynom p2 = new Polynom("+1x^3+3x^2+1x^0");
	      

	      //check for equality
	      assertEquals("+3x^5+2x^3+3x^2+2x^1+5", p1.addPol(p2).toString());
	      assertEquals("+3x^5-3x^2+2x^1+3", p1.subPol(p2).toString());
	      assertEquals("+3x^2-9x^1+28", p1.divPol(p2).get(0).toString());
	      assertEquals("-87x^2+11x^1-24", p1.divPol(p2).get(1).toString());
	      assertEquals("+3x^8+9x^7+x^6+6x^5+2x^4+11x^3+12x^2+2x^1+4", p1.mulPol(p2).toString());
	      assertEquals("+0.50x^6+0.25x^4+x^2+4x^1", p1.integratePol().toString());
	      assertEquals("+15x^4+3x^2+2", p1.derivPol().toString()); 
	      
	      
	      
	      
	 }
}
