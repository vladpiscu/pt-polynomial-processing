package Polynomial;

public class IllegalArguments extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalArguments(){
	}
	
	public IllegalArguments(String s)
	{
		super(s);
	}
}
