package Polynomial;
import java.util.*;

public class Polynom {
	private List<Monom> monoms;
	
	public Polynom()
	{
		monoms = new ArrayList<Monom>();
	}
	
	public Polynom(String s)
	{
		monoms = new ArrayList<Monom>();
		this.createPol(s);
	}
	
	public Polynom(Polynom p)
	{
		this.monoms = new ArrayList<Monom>(p.getMonoms());
		this.sortMonoms();
	}
	
	private void createPol(String s)
	{
		StringTokenizer st = new StringTokenizer(s, "+-", true);
		Monom m;
		String s1;
		while(st.hasMoreTokens())
		{
			s1 = st.nextToken();
			if(st.hasMoreTokens())
				s1 = s1.concat(st.nextToken());
			m = new Monom(s1);
			this.addMonom(m);
		}
		this.sortMonoms();
	}
	
	public void removeMonoms()
	{
		ArrayList<Monom> zeroMonoms = new ArrayList<Monom>();
		for(Monom m : monoms)
		{
			if(m.getCoeff().doubleValue() == 0)
				zeroMonoms.add(m);
		}
		monoms.removeAll(zeroMonoms);
	}
	
	public void addMonom(Monom monom)
	{
		Iterator<Monom> it = monoms.iterator();
		Monom m;
		boolean k = true;
		while(it.hasNext() && k)
		{
			m = it.next();
			if(m.equals(monom))
			{
				m.setCoeff(m.getCoeff().doubleValue() + monom.getCoeff().doubleValue());
				k = false;
			}
		}
		if(k)
		{
			monoms.add(monom);
		}
	}
	
	
	
	public String toString()
	{
		String s = new String();
		for(Monom m : monoms)
		{
			s = s.concat(m.toString());
		}
		return s;
	}
	
	public List<Monom> getMonoms()
	{
		return this.monoms;
	}
	
	private void sortMonoms()
	{
		MonomComp comp = new MonomComp();
		monoms.sort(comp);
	}
	
	public int getDegree()
	{
		if(monoms.isEmpty() == false)
			return monoms.get(0).getDegree();
		else
			return -1;
	}
	
	public Polynom addPol(Polynom pol)
	{
		Polynom p = new Polynom();
		for(Monom m : this.monoms)
		{
			p.addMonom(new Monom(m));
		}
		
		for(Monom m : pol.getMonoms())
		{
			p.addMonom(new Monom(m));
		}
		p.removeMonoms();
		p.sortMonoms();
		return p;
	}
	
	public Polynom subPol(Polynom pol)
	{
		Polynom p = new Polynom();
		for(Monom m : this.monoms)
		{
			p.addMonom(new Monom(m));
		}
		for(Monom m : pol.getMonoms())
		{
			Monom m1 = new Monom(m);
			m1.setCoeff(-m1.getCoeff().doubleValue());
			p.addMonom(m1);
		}
		p.removeMonoms();
		p.sortMonoms();
		return p;
	}
	
	public Polynom mulPol(Polynom pol)
	{
		Polynom p = new Polynom();
		for(Monom m1 : this.monoms)
		{
			for(Monom m2 : pol.getMonoms())
			{
				p.addMonom(new Monom(m1.getDegree() + m2.getDegree(), m1.getCoeff().doubleValue() * m2.getCoeff().doubleValue()));
			}
		}
		p.removeMonoms();
		p.sortMonoms();
		return p;
	}
	
	
	public List<Polynom> divPol(Polynom pol)
	{
		Polynom divident = new Polynom(this);
		Polynom divisor = pol;
		Polynom quotient = new Polynom();
		Polynom aux;
		if(divident.getMonoms().isEmpty() ==false && divisor.getMonoms().isEmpty() == false)
			while(divident.getDegree() >= divisor.getDegree())
			{
				Monom d = divident.getMonoms().get(0);
				int degree = d.getDegree() - divisor.getMonoms().get(0).getDegree();
				double coeff = d.getCoeff().doubleValue() / divisor.getMonoms().get(0).getCoeff().doubleValue();
				aux = new Polynom();
				for(Monom m : divisor.getMonoms())
				{
					aux.addMonom(new Monom(m.getDegree() + degree, m.getCoeff().doubleValue() * coeff));
				}
				divident = divident.subPol(aux);
				quotient.addMonom(new Monom(degree, coeff));
			}
		
		
		quotient.sortMonoms();
		List<Polynom> result = new ArrayList<Polynom>();
		result.add(quotient);
		result.add(divident);
		
		return result;
	}
	
	public Polynom derivPol()
	{
		Polynom p = new Polynom();
		for(Monom m : this.monoms)
		{
			if(m.getDegree() > 0)
				p.addMonom(new Monom(m.getDegree() - 1, m.getCoeff().doubleValue() * m.getDegree()));
		}
		p.removeMonoms();
		return p;
	}
	
	public Polynom integratePol()
	{
		Polynom p = new Polynom();
		for(Monom m : this.monoms){
			p.addMonom(new Monom(m.getDegree() + 1, m.getCoeff().doubleValue() / (m.getDegree() + 1)));
		}
		return p;
	}
	
	
}
