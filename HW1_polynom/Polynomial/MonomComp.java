package Polynomial;
import java.util.Comparator;

public class MonomComp implements Comparator<Monom>{
	MonomComp()
	{
		super();
	}
	
	public int compare(Monom m1, Monom m2)
	{
		if(m1.getDegree() > m2.getDegree())
			return -1;
		if(m1.getDegree() < m2.getDegree())
			return 1;
		return 0;
	}
	
}
