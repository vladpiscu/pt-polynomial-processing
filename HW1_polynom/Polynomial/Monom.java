package Polynomial;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class Monom{
	private int degree;
	private Number coeff;
	
	public Monom(int degree, Number coeff)
	{
		try{
			isException(degree);
			this.degree = degree;
			this.coeff = coeff;
		}
		catch(IllegalArguments e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	public Monom(Monom m)
	{
		this.degree = m.getDegree();
		this.coeff = m.getCoeff();
	}
	
	
	public Monom(String s)
	{
		try
		{	
			this.fromString(s);
		}
		catch(IllegalArguments e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	
	private void isException(int degree) throws  IllegalArguments
	{
		if(degree < 0)
		{
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal Degree");
		}

		
	}
	
	public void setDegree(int n)
	{
		degree = n;
	}
	
	public int getDegree()
	{
		return degree;
	}
	
	public Number getCoeff()
	{
		return coeff;
	}
	
	public void setCoeff(Number coeff)
	{
		this.coeff = coeff;
	}
	
	public String toString()
	{
		double coeff = this.coeff.doubleValue();
		DecimalFormat dc;
		if(coeff != (int) coeff)
			dc = new DecimalFormat("0.00");
		else
			dc = new DecimalFormat("0");
		String coeffString = dc.format(coeff);
		if(coeff == 0)
			return "";
		if(this.getDegree() == 0)
		{
			if(coeff > 0)
				return "+" + coeffString;
			return "" + coeffString;
		}
		if(coeff < 0)
		{
			if(coeff == -1)
				return "-x^" + this.getDegree();
			return coeffString + "x^" + this.getDegree();
		}
		if(coeff == 1)
			return "+x^" + this.getDegree();
		return "+" + coeffString + "x^" + this.getDegree();
			
	}
	
	public boolean equals(Object obj)
	{
		if(obj instanceof Monom)
		{
			Monom m = (Monom) obj;
			if(this.degree == m.getDegree())
				return true;
		}
		return false;
	}
	
	private void fromString(String s) throws IllegalArguments
	{
		int coeff = 0, degree = 0;
		char c;
		boolean negative = false;
		int i = 0;
		if(i >= s.length())
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		c = s.charAt(i);
		if(((c == '+') || (c == '-')) == false)
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		
		if(c == '-')
			negative = true;
		else
			negative = false;
		i++;
		if(i >= s.length() || (s.charAt(i) >= '0' && s.charAt(i) <= '9') == false)
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		c = s.charAt(i);
		while(c >= '0' && c <= '9')
		{
			coeff = coeff * 10 + c - '0';
			i++; 
			if(i >= s.length())
			{
				this.degree = 0;
				this.coeff = 0;
				JOptionPane.showMessageDialog(null, "Input is incorect!");
				throw new IllegalArguments("Illegal input");
			}
			c = s.charAt(i);
		}
		
		if(c != 'x' || i >= s.length())
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		i++;
		if(i >= s.length() || s.charAt(i) != '^')
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		c = s.charAt(i);
		i++; 
		if(i >= s.length() || (s.charAt(i) < '0' || s.charAt(i) > '9'))
		{
			this.degree = 0;
			this.coeff = 0;
			JOptionPane.showMessageDialog(null, "Input is incorect!");
			throw new IllegalArguments("Illegal input");
		}
		c = s.charAt(i);
		degree = degree * 10 + c - '0';
		i++;
		if(i < s.length())
			c = s.charAt(i);
		while(c >= '0' && c <= '9' && i < s.length())
		{
			degree = degree * 10 + c - '0';
			i++;
			if(i < s.length())
				c = s.charAt(i);
		}
		if(negative == true)
			coeff = -coeff;
		this.degree = degree;
		this.coeff = coeff;
	}
}
