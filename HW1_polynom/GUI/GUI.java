package GUI;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private final JButton btnAddition = new JButton("Addition");
	private final JTextField inputField1 = new JTextField();
	private final JTextField inputField2 = new JTextField();
	private final JButton btnSubstraction = new JButton("Substraction");
	private final JButton btnMultiplication = new JButton("Multiplication");
	private final JButton btnDivision = new JButton("Division");
	private final JButton btnIntegration = new JButton("Integration");
	private final JButton btnDerivation = new JButton("Derivation");
	private final JTextArea outputField = new JTextArea();
	
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Polynomial processing");
		inputField2.setText("+1x^3+3x^2+1x^0");
		inputField2.setFont(new Font("Tahoma", Font.PLAIN, 44));
		inputField2.setBounds(623, 135, 560, 60);
		inputField2.setColumns(10);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1251, 762);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		btnAddition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddition.setFont(new Font("Tahoma", Font.PLAIN, 44));
		
		
		btnAddition.setBounds(26, 246, 290, 60);
		panel.add(btnAddition);
		inputField1.setText("+3x^5+1x^3+2x^1+4x^0");
		inputField1.setFont(new Font("Tahoma", Font.PLAIN, 44));
		
		
		inputField1.setBounds(26, 135, 560, 60);
		panel.add(inputField1);
		inputField1.setColumns(10);
		
		panel.add(inputField2);
		btnSubstraction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSubstraction.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnSubstraction.setBounds(456, 246, 290, 60);
		
		panel.add(btnSubstraction);
		btnMultiplication.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnMultiplication.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnMultiplication.setBounds(866, 246, 317, 60);
		
		panel.add(btnMultiplication);
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDivision.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnDivision.setBounds(26, 371, 290, 60);
		
		panel.add(btnDivision);
		btnIntegration.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnIntegration.setBounds(456, 371, 290, 60);
		
		panel.add(btnIntegration);
		btnDerivation.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnDerivation.setBounds(866, 371, 317, 60);
		
		panel.add(btnDerivation);
		
		JLabel lblPolynom = new JLabel("Polynom 1:");
		lblPolynom.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblPolynom.setBounds(200, 51, 227, 69);
		panel.add(lblPolynom);
		
		JLabel lblPolynom_1 = new JLabel("Polynom 2:");
		lblPolynom_1.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblPolynom_1.setBounds(802, 51, 227, 69);
		panel.add(lblPolynom_1);
		
		JLabel lblResult = new JLabel("Result:");
		lblResult.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblResult.setBounds(13, 499, 227, 69);
		panel.add(lblResult);
		outputField.setColumns(10);
		
		outputField.setLineWrap(true);
		outputField.setFont(new Font("Monospaced", Font.PLAIN, 44));
		outputField.setBounds(160, 470, 1023, 139);
		panel.add(outputField);
		
		JLabel lblPleaseEnterEvery = new JLabel("Please enter every monomial in the form: +10x^5. Do not forget the sign and the power.");
		lblPleaseEnterEvery.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPleaseEnterEvery.setBounds(0, 15, 1209, 33);
		panel.add(lblPleaseEnterEvery);
	}
	
	public String getInput1()
	{
		return inputField1.getText();
	}
	
	public String getInput2()
	{
		return inputField2.getText();
	}
	
	public void setOutput(String s)
	{
		outputField.setText(s);
	}
	
	public void addSubListener(ActionListener a)
	{
		btnSubstraction.addActionListener(a);
	}
	
	public void addMulListener(ActionListener a)
	{
		btnMultiplication.addActionListener(a);
	}
	
	public void addDivListener(ActionListener a)
	{
		btnDivision.addActionListener(a);
	}
	
	public void addIntegrateListener(ActionListener a)
	{
		btnIntegration.addActionListener(a);
	}
	
	public void addDerivateListener(ActionListener a)
	{
		btnDerivation.addActionListener(a);
	}
	
	
	public void addAddListener(ActionListener a) {
		btnAddition.addActionListener(a);
	}
}
