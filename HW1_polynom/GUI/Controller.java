package GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Polynomial.Polynom;

public class Controller {
	private GUI g;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addAddListener(new Addition());
		this.g.addSubListener(new Substraction());
		this.g.addMulListener(new Multiplication());
		this.g.addIntegrateListener(new Integration());
		this.g.addDerivateListener(new Derivation());
		this.g.addDivListener(new Division());
	}
	
	public class Addition implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			Polynom p1 = new Polynom(g.getInput1());
			Polynom p2 = new Polynom(g.getInput2());
			g.setOutput(p1.addPol(p2).toString());
		}
	}
	
	
	public class Substraction implements ActionListener{
		public void actionPerformed(ActionEvent arg0)
		{
			Polynom p1 = new Polynom(g.getInput1());
			Polynom p2 = new Polynom(g.getInput2());
			g.setOutput(p1.subPol(p2).toString());
		}
	}
	
	public class Multiplication implements ActionListener{
		public void actionPerformed(ActionEvent arg0)
		{
			Polynom p1 = new Polynom(g.getInput1());
			Polynom p2 = new Polynom(g.getInput2());
			g.setOutput(p1.mulPol(p2).toString());
		}
	}
	
	public class Derivation implements ActionListener{
		public void actionPerformed(ActionEvent arg0)
		{
			Polynom p1 = new Polynom(g.getInput1());
			g.setOutput(p1.derivPol().toString());
		}
	}
	
	public class Integration implements ActionListener{
		public void actionPerformed(ActionEvent arg0)
		{
			Polynom p1 = new Polynom(g.getInput1());
			g.setOutput(p1.integratePol().toString());
		}
	}
	
	public class Division implements ActionListener{
		public void actionPerformed(ActionEvent arg0)
		{
			Polynom p1 = new Polynom(g.getInput1());
			Polynom p2 = new Polynom(g.getInput2());
			if(p2.getMonoms().isEmpty())
				g.setOutput("Error! Division by 0");
			else
				if(p1.getMonoms().isEmpty())
					g.setOutput("The quotient is: 0");
				else
					g.setOutput("The quotient is: " + p1.divPol(p2).get(0).toString() + "\nThe remainder is: " + p1.divPol(p2).get(1).toString());
		}
	}
}
